# Nino

Welcome to the documentation for Nino, an unofficial API wrapper for AniList API

## Supports

* `Ratelimiting` - Handles the API's ratelimiting.
* `Pagination` - Handles pagination, every thing in this wrapper is paginated.
* `Easy usage` - This API wrapper was designed to be used by the average user.

[Get started here](getting-started.md)