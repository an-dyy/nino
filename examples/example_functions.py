import typing as t

import asyncio
import nino


async def filter_animes(paginator: nino.PaginatedQuery, check: t.Callable) -> t.List[nino.Anime]:
    """
    filter_animes returns a list of animes that pass the check

    Args:
        paginator (nino.PaginatedQuery): The paginator instance
        check (t.Callable): The check to validate against

    Returns:
        list: List of Anime instances that pass the check
    """
    # Searches through the iterable and spits back instances that pass the check
    # A very useful method for utilitiy and other use cases, keep this method in mind
    return paginator.find(paginator.animes, check)


async def get_attributes(paginator: nino.PaginatedQuery, attribute: str) -> t.List:
    """
    get_attributes returns a list of the said attribute from avaliable instances

    Args:
        paginator (nino.PaginatedQuery): The paginator instance that holds all the information
        attribute (str): The attribute to look for

    Returns:
        list: List of the attribute's value for each instance
    """
    # Walks through the paginator and returns a list of said attribute from every instance avaliable
    # paginator.walk(attr) is a shortcut for paginator.walk(attr, paginator.animes)
    # By default the library automatically picks what to walk through for you when no iterable is passed
    return paginator.walk(attribute)


async def to_pag(anime: str, page=1, per_page=1) -> nino.PaginatedQuery:
    """
    to_list returns a list of animes

    Args:
        anime (str): What to search for
        page (int, optional): What page to be on. Defaults to 1.
        per_page (int, optional): How many items to show per page. Defaults to 1.

    Returns:
        list: List of anime instances
    """
    async with nino.Client() as client:
        paginator = await client.anime_search(anime, page=page, per_page=per_page)
        # Returns a paginator that is used for information and util.
        # anime_search takes in three arguments, the search string, page and then per_page.
        # page and per_page defaults to 1 so by default all searches are not paginated unless passed an int more then 1
        # Returning a paginator which contains the anime instances that represent an Anime
        return paginator

pagintor = asyncio.run(to_pag("Fate", page=1, per_page=3))
print(asyncio.run(get_attributes(pagintor, "id")))
print(asyncio.run(filter_animes(pagintor, lambda a: a.id != 74919)))
