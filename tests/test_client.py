from nino import Client
import pytest


@pytest.mark.asyncio
async def test_anime():
    async with Client() as client:
        paginator = await client.anime_search("Overlord")

    assert paginator.animes[0].id == 20832
    assert 20832 in paginator.walk("id")
    assert paginator.from_id(20832) == paginator.animes[0]


@pytest.mark.asyncio
async def test_character():
    async with Client() as client:
        paginator = await client.character_search("Nino")

    assert paginator.characters[0].id == 126372
    assert 126372 in paginator.walk("id")
    assert paginator.from_id(126372) == paginator.characters[0]


@pytest.mark.asyncio
async def test_staff():
    async with Client() as client:
        paginator = await client.staff_search("Kugane Maruyama")

    assert paginator.staffs[0].id == 119069
    assert 119069 in paginator.walk("id")
    assert paginator.from_id(119069) == paginator.staffs[0]


@pytest.mark.asyncio
async def test_user():
    async with Client() as client:
        paginator = await client.user_search("andyyyyyyy")

    assert paginator.users[0].id == 5455050
    assert 5455050 in paginator.walk("id")
    assert paginator.from_id(5455050) == paginator.users[0]
